document.addEventListener("DOMContentLoaded", () => {
    const getAlltournament = async () => {
      try {
        const res = await axios.get("https://mlbb-tournament-hosting.onrender.com/tournament");
        console.log(res.data.data);
        showTournament(res.data.data);
      } catch (err) {
        alert(err.message);
      }
    };
  
    function getNthSuffix(day) {
      if (day > 3 && day < 21) return ''; // Special case for 11th, 12th, 13th
      switch (day % 10) {
          case 1: return '';
          case 2: return '';
          case 3: return '';
          default: return '';
      }
  }

  function formatDate(dateString) {
      const date = new Date(dateString);
      const day = date.getDate();
      const month = date.getMonth() + 1; // Months are zero-indexed
      const year = date.getFullYear();
      const nth = getNthSuffix(day);

      return `${day}${nth}/${month}/${year}`;
  }

  const showTournament = async (home) => {
      var event_cards = document.querySelector(".event_cards");
      home.forEach((home) => {
          const formattedDate = formatDate(home.date);
          event_cards.innerHTML += `
              <a href="/Event_details?tournamentid=${home._id}">
                  <div class="card">
                      <img src="admin views/img/florian-olivo-Mf23RF8xArY-unsplash.jpg" alt="">
                      <h3 style="color: #8A2BE1;">${home.tournamentName}</h3>
                      <div class="card-body">
                          <div class="card-body0">
                              <p><strong>Status:</strong><span style="color: #3DB8FB;">${home.status}</span></p>
                              <p><strong>Date:</strong> ${formattedDate}</p>
                          </div>
                          <div class="card-body1">
                              <p><strong>Prize:</strong>${home.prize}</p>
                              <p><strong>Durations:</strong> ${home.duration}</p>
                          </div>
                      </div>
                  </div>
              </a>
          `;
      });
  };

  
    getAlltournament();
  });
  