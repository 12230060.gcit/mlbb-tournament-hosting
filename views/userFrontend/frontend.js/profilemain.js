const tokenCookie = document.cookie
  .split(";")
  .find((cookie) => cookie.trim().startsWith("token="));
const token = tokenCookie ? tokenCookie.split("=")[1] : null;
const dToken = JSON.parse(token);
const profileContainer = document.getElementById("username");
profileContainer.textContent = dToken.user.name;
console.log(dToken.user);

async function displayProfile(token) {
  const decodedToken = parseJwt(token);
  
  profileContainer.textContent = `
    <div class="name" id="username">${username}</div><br>
        <div class="team">${teams}</div>      

    `;

  profileContainer.innerHTML = profileHTML;
}

function parseJwt(token) {
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  const jsonPayload = decodeURIComponent(
    atob(base64)
      .split("")
      .map((c) => "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2))
      .join("")
  );

  return JSON.parse(jsonPayload);
}

