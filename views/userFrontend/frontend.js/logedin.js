// Function to fetch user data
const getUserData = async (userId) => {
    try {
      const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/userdb/${userId}`);
      return res.data; // Assuming user data is in res.data
    } catch (err) {
      throw new Error(err.message);
    }
  };
  
  // Parse JWT token from cookie
  const tokenCookie = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("token="));
  const token = tokenCookie ? tokenCookie.split("=")[1] : null;

  if (token) {
    document.getElementById("username").innerHTML = JSON.parse(token).user.name;

    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    
    // Access user ID from decoded token
    const userId = decodedToken.id;
  
    // Call getUserData and wait for it to complete
    getUserData(userId)
      .then((userData) => {
        // Update UI with user data, e.g., display user's name
        document.getElementById("username").innerHTML = userData.data.name;
      })
      .catch((error) => {
        console.error("Error fetching user data:", error);
        // Handle error
      });
  } else {
    console.log("JWT token not found in cookies");
    // Display default username in HTML element
    document.getElementById("username").innerHTML = "Username";
  };
  