// Function to display success message
function showSuccess(message) {
  iziToast.success({
    title: 'Login successful',
    message: message,
    position: 'topRight',
    timeout: 1500, // 3 seconds
  });
}

// Function to display error message
function showError(message) {
  iziToast.error({
    title: 'Error:',
    message: message,
    position: 'topRight',
    timeout: 3000, // 3 seconds
});
}

document.addEventListener("DOMContentLoaded", () => {
  const loginForm = document.getElementById("loginForm");

  loginForm.addEventListener("submit", async (e) => {
    e.preventDefault();
    const email = document.getElementById("emailInput").value;
    const password = document.getElementById("passwordInput").value;

    const url = isAdminLogin(email) ? "https://mlbb-tournament-hosting.onrender.com/admindb/login" : "https://mlbb-tournament-hosting.onrender.com/userdb/login";

    try {
      const res = await axios.post(url, {
        email,
        password,
      });

      if (res.status === 200) {
        const token = res.data.data;
        const isAdmin = isAdminLogin(email);
        document.cookie = 'token=' + JSON.stringify(token);
        document.cookie = 'isAdmin=' + JSON.stringify(isAdmin);
        showSuccess("Login Success")
        setTimeout(()=> {
          
          const redirectUrl = isAdmin ? "/adminhome" : "/mainDashboard";
          window.location.href = redirectUrl;
        },1500)
      }
    } catch (err) {
      console.error(err);
      showError("Login failed: " + "Incorrect email or password.");
    }
  });
  
  // Function to determine if admin login
  function isAdminLogin(email) {
    const adminEmail = "mlbbadmin@gmail.com"
    return email ===adminEmail
    // return email.includes("@gmail.com"); // Change this to your actual admin email domain
}
});
