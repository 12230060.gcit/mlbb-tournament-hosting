document.addEventListener("DOMContentLoaded", async () => {
    const getSchedule = async () => {
        try {
            const urlParams = new URLSearchParams(window.location.search);
            const tournamentid = urlParams.get("tournamentid");
            console.log("tournamentid:", tournamentid);
            if (!tournamentid) {
                throw new Error("tournamentid parameter is missing");
            }
            
            const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament/${tournamentid}`);
            showSchedule(res.data.data);
        } catch (err) {
            console.log(err.message);
        }
    };
  
    const showSchedule = (information) => {
        console.log(information);
        try {
            const scheduleInfo = document.querySelector(".scheduleContainer");
            if (scheduleInfo) { 
                scheduleInfo.innerHTML = ''; // Clear existing content
                information.schedule.forEach(element => {
                    scheduleInfo.innerHTML += `
                    <div class="schedule">
                    <div class="team">
                        <img src="/userFrontend/img/team.svg" alt="">
                        <p>${element.teamName}</p>
                    </div>
                    <p>VS</p>
                    <div class="team">
                        <p>${element.teamName}</p>
                        <img src="/userFrontend/img/logoTeam2.png" alt="">
                    </div>
                    `;
                });
            } else {
                console.log("announcementInfo element not found");
            }
            
            const navContainer = document.querySelector(".linkWrapper");
            if (navContainer) {
                navContainer.innerHTML = `
                    <a href="/tournamentOverview?tournamentid=${information._id}" class="overview"><span class="material-symbols-outlined">overview_key</span>Overview</a>
                    <a href="/tournamentSchedule?tournamentid=${information._id}"class="overview"><span class="material-symbols-outlined">calendar_month</span>Schedule</a>
                    <a href="/tournamentLivestream?tournamentid=${information._id}"><span class="material-symbols-outlined">campaign</span>Live Streams</a>
                    <a href="/tournamentAnnouncement?tournamentid=${information._id}"><span class="material-symbols-outlined">smart_display</span>Announcements</a>
                `;
            } else {
                console.log("navContainer element not found");
            }
        } catch (err) {
            console.log(err.message);
        }
    };
  
    getSchedule();
});
