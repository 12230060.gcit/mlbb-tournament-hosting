document.addEventListener("DOMContentLoaded", async () => {
    const getliveStreams = async () => {
        try {
            const urlParams = new URLSearchParams(window.location.search);
            const tournamentid = urlParams.get("tournamentid");
            console.log("tournamentid:", tournamentid);
            if (!tournamentid) {
                throw new Error("tournamentid parameter is missing");
            }
            
            const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/${tournamentid}`);
            const information = res.data.data;
            showStreams(information.liveStreams);

            const navContainer = document.querySelector(".linkWrapper");
            if (navContainer) {
                navContainer.innerHTML = `
                    <a href="/tournamentOverview?tournamentid=${information._id}"><span class="material-symbols-outlined">overview_key</span>Overview</a>
                    <a href="/tournamentSchedule?tournamentid=${information._id}"><span class="material-symbols-outlined">calendar_month</span>Schedule</a>
                    <a href="/tournamentLivestream?tournamentid=${information._id}" class="active"><span class="material-symbols-outlined">campaign</span>Live Streams</a>
                    <a href="/tournamentAnnouncement?tournamentid=${information._id}"><span class="material-symbols-outlined">smart_display</span>Announcements</a>
                `;
            } else {
                console.log("navContainer element not found");
            }

        } catch (err) {
            console.log(err.message);
        }
    };

    const showStreams = async (streams) => {
        try {
            var liveStreamWrapper = document.querySelector('.liveStreamWrapper');
            console.log(liveStreamWrapper);
            console.log(streams);

            liveStreamWrapper.innerHTML = '';

            for (const stream of streams) {
                // Fetch team names corresponding to team1 and team2 ObjectIds
                const team1Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/teams/${stream.team1}`);
                const team2Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/teams/${stream.team2}`);
       
                // Access team names from the response data
                const team1Name = team1Res.data.data.team.teamName;
                const team2Name = team2Res.data.data.team.teamName;
                liveStreamWrapper.innerHTML +=
                    `<div class="livestreamContainer">
                        <div class="teamDetails">
                            <div class="team">
                                <p>${team1Name}</p>
                            </div>
                            <div class="link">
                                <p>VS</p>
                                <a href="${stream.streamLink}">Watch Live</a>
                            </div>
                            <div class="team">
                                <p>${team2Name}</p>
                            </div>
                        </div>
                    </div>`;
            }
        } catch (err) {
            console.log(err.message);
        }
    };
  
    getliveStreams();
});
