const tokenCookie = document.cookie
  .split(";")
  .find((cookie) => cookie.trim().startsWith("token="));
const token = tokenCookie ? tokenCookie.split("=")[1] : null;
const dToken = JSON.parse(token);
const profileContainer = document.getElementById("username");
profileContainer.textContent = dToken.user.name;
console.log(dToken.user);
{

  profileContainer.innerHTML = `
  <div class="name" id="username">${dToken.user.name}</div><br>   
  `;
}

document.addEventListener("DOMContentLoaded", () => {
  const getTournamentDetail = async () => {
    try {
      var tournamentid = dToken.user.teams
      // Now you can use the tournamentid to make the request
      const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament`);
      showTournament(res.data.data);
    } catch (err) {
      console.log(err.message);
    }
  };
  getTournamentDetail();

  const showTournament = (data) => { // Accept userid as an argument
    const showTournamentContainer = document.querySelector('.contents');
    console.log(data)
    data.forEach(tournament => {
      tournament.teams.forEach(teams => {
        console.log(teams)
        console.log(dToken.user.teams)
        if (teams._id === dToken.user.teams) {
          showTournamentContainer.innerHTML = `
          <div class="card">
            <img src="/userFrontend/img/ella-don-LBQdL30Ywuw-unsplash.jpg" alt="" >
            <h3>${tournament.tournamentName}</h3>
          <div class="card-body">
            <div class="card-body0">
                <p>Status:<span>${tournament.status}</span></p>
                <p>${tournament.date}</p>
                <div class="link"><a href="/tournamentOverview?tournamentid=${tournament._id}">View Tourament Dashboard</a></div>
            </div>
            
          `
        }

      })


    });

  };
});

