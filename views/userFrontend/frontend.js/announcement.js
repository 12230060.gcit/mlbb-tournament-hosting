document.addEventListener("DOMContentLoaded", async () => {
    const getTournamentAnnouncement = async () => {
        try {
            const urlParams = new URLSearchParams(window.location.search);
            const tournamentid = urlParams.get("tournamentid");
            console.log("tournamentid:", tournamentid);
            if (!tournamentid) {
                throw new Error("tournamentid parameter is missing");
            }
            
            const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/${tournamentid}`);
            showAnnouncement(res.data.data);
        } catch (err) {
            console.log(err.message);
        }
    };
  
    const showAnnouncement = (information) => {
        console.log(information);
        try {
            const announcementInfo = document.querySelector(".announcementWrapper");
            if (announcementInfo) { 
                announcementInfo.innerHTML = ''; // Clear existing content
                information.announcement.forEach(element => {
                    announcementInfo.innerHTML += `
                        <div class="announcement">
                            <h1>${element.title}</h1>
                            <p>${element.para}</p>
                        </div>
                    `;
                });
            } else {
                console.log("announcementInfo element not found");
            }
            
            const navContainer = document.querySelector(".linkWrapper");
            if (navContainer) {
                navContainer.innerHTML = `
                    <a href="/tournamentOverview?tournamentid=${information._id}"class="overview"><span class="material-symbols-outlined">overview_key</span>Overview</a>
                    <a href="/tournamentSchedule?tournamentid=${information._id}"><span class="material-symbols-outlined">calendar_month</span>Schedule</a>
                    <a href="/tournamentLivestream?tournamentid=${information._id}"><span class="material-symbols-outlined">campaign</span>Live Streams</a>
                    <a href="/tournamentAnnouncement?tournamentid=${information._id}"class="overview"><span class="material-symbols-outlined">smart_display</span>Announcements</a>
                `;
            } else {
                console.log("navContainer element not found");
            }
        } catch (err) {
            console.log(err.message);
        }
    };
  
    getTournamentAnnouncement();
});
