document.addEventListener("DOMContentLoaded", async () => {
  // Parse JWT token from cookie
  const tokenCookie = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("token="));
  const token = tokenCookie ? tokenCookie.split("=")[1] : null;
  const userId = JSON.parse(token).user._id;

  try {
    const res = await axios.get("https://mlbb-tournament-hosting.onrender.com/teams");
    console.log(res.data.data);
    showTeams(res.data.data.teams, userId);
  } catch (err) {
    showError(err.message);
  }
});

function showTeams(teams, userId) {
  var teamcontainer = document.querySelector(".team-container");
  teams.forEach((team) => {
    var ismember = false; // Declare ismember locally for each team
    team.members.forEach((member) => {
      console.log(member._id);
      if (member._id == userId) {
        ismember = true;
      }
    });

    // Reset ismember to false for each team iteration
    if (!ismember) {
      teamcontainer.innerHTML += `<div class="card">
              <img src="/userFrontend/img/Group 534.png" alt="Card Image"><hr>
              <div class="card-text">
                  <h3>${team.teamName}</h3>
                  <p>Members: ${team.members.length}</p>
                  <a onclick="joinTeam('${team._id}', '${userId}')">Apply</a>     
              </div>
          </div>`;
    }
  });
}

async function joinTeam(id, userId) {
  // Display a confirmation dialog
  const confirmed = window.confirm("Are you sure you want to join this team?");

  if (confirmed) {
    console.log("User confirmed to join the team:", id);

    try {
      const teamRes = await axios.patch("https://mlbb-tournament-hosting.onrender.com/teams/" + id, {
        members: [userId],
      });
      console.log("Response after joining team:", teamRes.data);

      const userRes = await axios.patch(
        "https://mlbb-tournament-hosting.onrender.com/userdb/" + userId,
        {
          teams: id,
        }
      );
      console.log("Response after updating user:", userRes.data);

      if (teamRes.status === 200 && userRes.status === 200) {
        window.location.href = "/profileInfo";
      }
    } catch (err) {
      showError(err.message);
    }
  } else {
    // If user cancels, log a message
    console.log("User cancelled joining the team.");
  }
}
