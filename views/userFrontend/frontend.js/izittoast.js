// Function to display success message
function showSuccess(message) {
    iziToast.success({
      title: 'Success : ',
      message: message,
      position: 'topRight',
      timeout: 3000, // 3 seconds
    });
  }
  
  // Function to display error message
  function showError(message) {
    iziToast.error({
      title: 'Error : ',
      message: message,
      position: 'topRight',
      timeout: 3000, // 3 seconds
});
}