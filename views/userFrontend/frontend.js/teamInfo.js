document.addEventListener("DOMContentLoaded", async function () {
    // Function to open the popup
    function openPopup() {
      document.getElementById("popup").style.display = "block";
    }
  
    // Function to close the popup
    function closePopup() {
      document.getElementById("popup").style.display = "none";
    }
  
    const tokenCookie = document.cookie
      .split(";")
      .find((cookie) => cookie.trim().startsWith("token="));
    const token = tokenCookie ? tokenCookie.split("=")[1] : null;
    const dToken = JSON.parse(token);
    const teamContainer = document.getElementById("username");
    teamContainer.textContent = dToken.user.name;
    console.log(dToken.user._id);
  
    // Function to fetch teams
    async function fetchTeams() {
      try {
        const response = await axios.get("https://mlbb-tournament-hosting.onrender.com/teams");
        const teams = response.data.data.teams;
  
        const teamContainer = document.querySelector(".content");
        teamContainer.innerHTML = ""; // Clear existing content
  
        console.log(teams);
        teams.forEach((team) => {
          var userid = "";
          team.members.forEach((member) => {
            if (dToken.user._id === member._id) {
              userid = member._id;
            }
          });
          if (userid === dToken.user._id) {
            const teamHTML = `
              <div class="heading">
                  <img src="/userFrontend/img/logoTeam-01 1.svg" alt="">
                  <div class="team">
                      <p>Team:<h3>${team.teamName}</h3></p>
                      
                  </div>
              </div>
              <div id='team-membercontainer'></div>`;
            teamContainer.insertAdjacentHTML("beforeend", teamHTML);
  
            const memberContainer = document.getElementById("team-membercontainer");
            if (team.members.length === 0) {
              memberContainer.innerHTML = "<p>No members in this team.</p>";
            } else {
              team.members.forEach((member) => {
                const memberHTML = `
                  <div class="members" >
                      <h4>Members</h4>
                      <div class="row-1">
                          <div class="fst">
                              <img src="/userFrontend/img/pro.svg" alt="">
                              <p>${member.name}</p>
                          </div>
                      </div>
                  </div>`;
                memberContainer.insertAdjacentHTML("beforeend", memberHTML);
              });
            }
          }
        });
      } catch (error) {
        console.error("Error fetching teams:", error);
      }
    }
  
    fetchTeams();
  });
  