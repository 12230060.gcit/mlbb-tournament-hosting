
document.addEventListener("DOMContentLoaded", () => {
  const getTournamentDetail = async () => {
    try {
      // Parse the URL to extract the homeId parameter
      const urlParams = new URLSearchParams(window.location.search);
      const tournamentid = urlParams.get("tournamentid").toString();
      console.log(tournamentid);
      // Now you ca n use the homeId to make the request
      const res = await axios.get(
        `https://mlbb-tournament-hosting.onrender.com/${tournamentid}`
      );
      showTournament(res.data.data);
    } catch (err) {
      console.log(err.message);
    }
  };

  const showTournament = (Information) => {
    console.log(Information);
    const eventName = document.querySelector(".top");
    const memberInfo = document.querySelector(".paragraph");
    const dateInfo = document.querySelector(".para");
    dateInfo.innerHTML = `
              <p>
                  ${Information.paragraph}
              </p>
          `;
    eventName.innerHTML = Information.tournamentName;
    memberInfo.innerHTML = `
        <p><span class="material-symbols-outlined">emoji_events</span> NU: ${Information.prize}</p>
        <p><span class="material-symbols-outlined">schedule</span> Duration: ${Information.duration}</p>
        <p><span class="material-symbols-outlined">calendar_month</span> Date: ${Information.date}</p>
        <p><span class="material-symbols-outlined">account_tree</span> Format: ${Information.tournamentType}</p>
      `;
    Information.textContent = Information.description;

    // const registerBtn = document.getElementById("registerBtn");
    // registerBtn.addEventListener("click", () => {
    //   window.location.href = "/register";
  };

  getTournamentDetail();
});
const tokenCookie = document.cookie
  .split(";")
  .find((cookie) => cookie.trim().startsWith("token="));
const token = tokenCookie ? tokenCookie.split("=")[1] : null;
const dToken = JSON.parse(token);
console.log(dToken.user.teams);

async function displayProfile(token) {
  const decodedToken = parseJwt(token);}


const urlParams = new URLSearchParams(window.location.search);
const tournamentid = urlParams.get("tournamentid").toString();

console.log(tournamentid);
document.getElementById("registerBtn").addEventListener("click", async (event) => {
  event.preventDefault();
  const confirmed = confirm(
    "Are you sure you want to register for this tournament?"
  );
  if (confirmed) {
    console.log(tournamentid); // Assuming tournamentid is defined somewhere

    var obj;

if (document.cookie) {
  obj = JSON.parse(document.cookie.substring(6));
} else {
  obj = JSON.parse("{}");
}


    // Using id manually 
    createTourn(tournamentid, dToken.user.teams);
    // createTourn(tournamentid, document._id);
  } else {
  }
});



async function createTourn(tournamentId, teamid) {
  console.log(teamid);
  const url = "https://mlbb-tournament-hosting.onrender.com/"+tournamentId;
  const data = {
    teams: teamid,
  };

  try {
    const response = await fetch(url, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });

    if (!response.ok) {
      throw new Error("Failed to register tournament");
    }

    const result = await response.json();
    console.log("Tournament registered successfully:", result);
    showSuccess("Tournament registered successfully:", result);
  } catch (error) {
    console.error("Error registering tournament:", error.message);
    showError("Error registering tournament:", error.message);
  }
}
