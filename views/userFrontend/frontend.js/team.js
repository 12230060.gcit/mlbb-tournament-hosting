document.addEventListener("DOMContentLoaded", function () {
  function openPopup() {
    document.getElementById("popup").style.display = "block";
  }

  function closePopup() {
    document.getElementById("popup").style.display = "none";
  }
  // Get the value of the cookie
  const cookieValue = document.cookie
    .split("; ")
    .find((row) => row.startsWith("token="));

  // Parse the JSON string from the cookie value
  const tokenJSON = cookieValue.split("=")[1];
  const token = JSON.parse(tokenJSON);

  // Function to handle team creation
  async function createNewTeam(event) {
    event.preventDefault(); // Prevent default form submission

    try {
      // Get form data
      const teamName = document.getElementById("teamName").value;
      // const logoFile = document.getElementById('logo').files[0];
      // const teamMembers = document.getElementById("username");
      const member = token.user._id;

      const formData = new FormData();

      formData.append("teamName", teamName);
      formData.append("members", member);
      console.log(formData.get("teamName"));
      console.log(formData.get("members"));
      const data = {
        teamName: teamName,
        members: member,
      };

      // formData.append('logo', logoFile);
      const response = await axios.post("https://mlbb-tournament-hosting.onrender.com/teams", {
        teamName: teamName,
        members: member,
      });

      var id = response.data.data.team._id;
      console.log(response.data);
      console.log(id);

      if (response.status === 201) {
        const responsePatch = await axios.patch(
          "https://mlbb-tournament-hosting.onrender.com/userdb/" +token.user._id,
          {
            teams: id,
          }
        );
        
        if (responsePatch.status === 201) {
          window.location.href = "/profileInfo";
        }
      }
    } catch (error) {
      console.error("Team creation failed:", error);
      alert("Team creation failed: " + error.response.data.error);
    }

    closePopup();
  }

  document.getElementById("popup").addEventListener("submit", createNewTeam);
});
