document.addEventListener("DOMContentLoaded", async () => {
    const getTournamentOverview = async () => {
        try {
            const urlParams = new URLSearchParams(window.location.search);
            const tournamentid = urlParams.get("tournamentid");
            if (!tournamentid) {
                throw new Error("tournamentid parameter is missing");
            }
            
            const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament/${tournamentid}`);
            showTournament(res.data.data);
        } catch (err) {
            console.log(err.message);
        }
    };
  
    const showTournament = (information) => {
        try {
            const tournamentInfo = document.querySelector(".detailsSection");
            const teamRegistered = document.querySelector(".teamWrapper");
            const dateInfo = document.querySelector(".description");

            if (dateInfo) {
                dateInfo.innerHTML = `<p>${information.description}</p>`;
            } else {
                console.log("dateInfo element not found");
            }

            const formatDate = (dateString) => {
                const options = { year: 'numeric', month: 'long', day: 'numeric' };
                const date = new Date(dateString);
                return date.toLocaleDateString(undefined, options);
            };

            const formattedDate = formatDate(information.date);

            if (tournamentInfo) {
                tournamentInfo.innerHTML = `
                    <p><span class="material-symbols-outlined">emoji_events</span><span class="editable">${information.prize}</span></p>
                    <p><span class="material-symbols-outlined">schedule</span><span class="editable">${information.duration}</span></p>
                    <p><span class="material-symbols-outlined">calendar_month</span><span class="editable">${formattedDate}</span></p>
                    <p><span class="material-symbols-outlined">account_tree</span><span class="editable">${information.tournamentType}</span></p>
                `;
            } else {
                console.log("tournamentInfo element not found");
            }

            if (teamRegistered) {
                teamRegistered.innerHTML = ""; 
                information.teams.forEach(team => {
                    teamRegistered.innerHTML += `
                        <li>
                            <img src="/admin views/img/logoTeam-01 1.png" alt="">
                            ${team.teamName}
                        </li>
                    `;
                });
            } else {
                console.log("teamRegistered element not found");
            }

            const navContainer = document.querySelector(".linkWrapper");
            if (navContainer) {
                navContainer.innerHTML = `
                    <a href="/tournamentOverview?tournamentid=${information._id}" class="overview"><span class="material-symbols-outlined">overview_key</span>Overview</a>
                    <a href="/tournamentSchedule?tournamentid=${information._id}"><span class="material-symbols-outlined">calendar_month</span>Schedule</a>
                    <a href="/tournamentLivestream?tournamentid=${information._id}"><span class="material-symbols-outlined">campaign</span>Live Streams</a>
                    <a href="/tournamentAnnouncement?tournamentid=${information._id}"><span class="material-symbols-outlined">smart_display</span>Announcements</a>
                `;
            } else {
                console.log("navContainer element not found");
            }
        } catch (err) {
            console.log(err.message);
        }
    };
  
    getTournamentOverview();
});
