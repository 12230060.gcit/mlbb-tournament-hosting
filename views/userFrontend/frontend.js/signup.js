document.addEventListener("DOMContentLoaded", () => {
  const saveBtn = document.getElementById("saveBtn");
  console.log(saveBtn);
  saveBtn.addEventListener("click", (e) => {
    e.preventDefault();
    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;
    const passwordConfirm = document.getElementById("confirm-password").value;
    // Enter validation function call or code here

    //

    addUser(name, email, password, passwordConfirm);
  });

  const addUser = async (name, email, password, passwordConfirm) => {
    try {
      const res = await axios.post("https://mlbb-tournament-hosting.onrender.com/userdb", {
        name,
        email,
        password,
        passwordConfirm,
      });
      if (res.status === 200) {
        showSuccess("User Added Successfully!");
        setTimeout(() => {
          window.location.href = "/tournamentLogin"; // Redirect to the login page after 2 seconds
        }, 2000); // 2000 milliseconds = 2 seconds
      }
    } catch (err) {
      console.log(err);
      showError("Please provide a valid email or a password");
    }
  };
});
