var obj = JSON.parse(document.cookie.substring(6));
console.log(obj);


export const updateSettings = async (data, type) => {
    try {
        const url =
            type === 'password'
                ? 'https://mlbb-tournament-hosting.onrender.com/userdb/updateMyPassword'
                : 'https://mlbb-tournament-hosting.onrender.com/userdb/updateMe'
    
        const res = await axios({
            method: 'PATCH',
            url,
            data,
        })
        console.log(res.data.status)
        if (res.data.status === 'success') {
            alert('success', 'Data updated successfully!')
        }
    } catch (err) {
        let message =
            typeof err.response !== 'undefined'
                ? err.response.data.message
                : err.message
        // showAlert('error', 'Error: Please provide valid email address', message)
        // showAlert('error', err.response.data.message)
        alert('error', err.response.data.message)
    }
}

const userDataForm = document.querySelector('.snd-col')
userDataForm.addEventListener('submit', (e) => {
    e.preventDefault()
    var obj = JSON.parse(document.cookie.substring(6))
    // console.log(obj.user._id)
    const form = new FormData()
    form.append('name', document.getElementById('name').value)
    // form.append('photo', document.getElementById('photo').files[0])
    form.append('userId', obj.user._id)
    console.log(form.get("name"), form.get("userId"))
    updateSettings(form, 'data')
})

const userPasswordForm = document.querySelector('form.form-user-password')
userPasswordForm.addEventListener('submit', async (e) => {
    e.preventDefault()

    // document.querySelector('.btn-save-password').textContent = 'Updating...'
    const passwordCurrent = document.getElementById('password-current').value
    const password = document.getElementById('password').value
    const passwordConfirm = document.getElementById('password-confirm').value
    await updateSettings(
        { passwordCurrent, password, passwordConfirm },
        'password',
    )
    // document.querySelector('.btn--save-password').textContent = 'Save password'
    document.getElementById('password-current').value = ''
    document.getElementById('password').value = ''
    document.getElementById('password-confirm').value = ''
})


const tokenCookie = document.cookie
    .split(";")
    .find((cookie) => cookie.trim().startsWith("token="));
const token = tokenCookie ? tokenCookie.split("=")[1] : null;
const dToken = JSON.parse(token)
const profileContainer = document.getElementById("username");
profileContainer.textContent = dToken.user.name
console.log(dToken.user.name)

function displayProfile(token) {

    const decodedToken = parseJwt(token);


    
   profileContainer.textContent = `
    <div class="name" id="username">${username}</div><br>
        <div class="team">${teams}</div>      

    `;

    profileContainer.innerHTML = profileHTML;
}

function parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''));

    return JSON.parse(jsonPayload);
}


// displayProfile(profileToken);

