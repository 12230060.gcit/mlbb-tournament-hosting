document.addEventListener("DOMContentLoaded", async () => {

    
const showAnnouncement = async (announcements) => {
    try {
        var announcementWrapper = document.querySelector('.tournament-section-wrapper');
        console.log(announcementWrapper)
        console.log(announcements)
        announcementWrapper.innerHTML = ''; // Clear previous content
        announcements.forEach(announcement => {
            announcementWrapper.innerHTML +=
                `<section class=" tournament-section">
                <h2 contenteditable="false">${announcement.title}</h2>
                <p contenteditable="false">${announcement.para}</p>
            </section>`;
        });
    } catch (err) {
        console.log(err.message);
       
    }
};
    try {
        const urlParams = new URLSearchParams(window.location.search);
        const tournamentid = urlParams.get("tournamentid");
        if (!tournamentid) {
            throw new Error("tournamentid parameter is missing");
        }

        console.log(tournamentid);
        const tournamentRes = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament/${tournamentid}`);
        const information = tournamentRes.data.data;

        // Populate the navigation links using the tournament information
        const navContainer = document.querySelector(".inline-nav");
        navContainer.innerHTML = `
            <a href="/editoverview?tournamentid=${information._id}">Overview</a>
            <a href="/Admin_structure?tournamentid=${information._id}">Structure</a>
            <a href="/Admin_Schedule?tournamentid=${information._id}">Schedule</a>
            <a href="/stream?tournamentid=${information._id}">Live Streams</a>
            <a href="/announcement?tournamentid=${information._id}"class="overview">Announcements</a>
        `;

        // Call the function to get and display tournament announcements
        console.log(information)
        await showAnnouncement(information.announcement);
    } catch (err) {
        console.log(err.message);
    }


const saveBtn = document.getElementById('saveBtn')
saveBtn.addEventListener("click" , () => {
    const title = document.getElementById('title').value;
    const para = document.getElementById('para').value;

    var data = {
        "title" : title,
        "para" : para
       }
    addAnnouncement(data)
   
})

const addAnnouncement = async (data) => {
    const urlParams = new URLSearchParams(window.location.search);
   var tournamentid=  urlParams.get("tournamentid");
    try {
        var res = await axios.post(`https://mlbb-tournament-hosting.onrender.com/annouc`, data);
        console.log(res.data.data._id);
        if (res.data.message === "Announcement created") {
          showSuccess("Announcement Added Successfully!");
        }

        var res = await axios.patch('https://mlbb-tournament-hosting.onrender.com/tournament/' + tournamentid , {
            "announcement"  : res.data.data._id
        })

        console.log(res.data)
        if(!res.data.status === "success"){
            showError("Announcement Failed To link in Tournament")
        }
        
      } catch (err) {
        console.log("Bad messafge");
        showError(err);
      }
}

});
