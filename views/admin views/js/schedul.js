document.addEventListener("DOMContentLoaded", async () => {

    const showSchedule = async (Schedule) => {
        try {

            var ScheduleWrapper = document.querySelector('.content-2');
            console.log(ScheduleWrapper);

            ScheduleWrapper.innerHTML = '';

            for (const schedules of Schedule) {
       
                // Fetch team names corresponding to team1 and team2 ObjectIds
                const team1Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/${schedules.team1}`);
                const team2Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/${schedules.team2}`);
       
                // Access team names from the response data
                // console.log(team2Res.data.data.team.teamName)
                const team1Name = team1Res.data.data.team.teamName;
                const team2Name = team2Res.data.data.team.teamName;
                ScheduleWrapper.innerHTML +=
                    `<div class="card">
                    <p class="team">${team1Name}</p>
                    <p class="vs">Vs</p>
                    <p class="team2">${team2Name}</p>
                    <input type="time" id="timeInput" name="timeInput" value="12:00">                
                </div>`;
            }
        } catch (err) {
            console.log(err.message);
        }
    };



    try {
        const urlParams = new URLSearchParams(window.location.search);
        const tournamentid = urlParams.get("tournamentid");
        if (!tournamentid) {
            throw new Error("tournamentid parameter is missing");
        }

        console.log(tournamentid);
        const tournamentRes = await axios.get(`http://localhost:6969/tournament/${tournamentid}`);
        const information = tournamentRes.data.data;

        // Populate the navigation links using the tournament information
        const navContainer = document.querySelector(".inline-nav");
        navContainer.innerHTML = `
                <a href="/editoverview?tournamentid=${information._id}">Overview</a>
                <a href="/Admin_structure?tournamentid=${information._id}">Structure</a>
                <a href="/Admin_Schedule?tournamentid=${information._id}"class="overview">Schedule</a>
                <a href="/stream?tournamentid=${information._id}">Live Streams</a>
                <a href="/announcement?tournamentid=${information._id}">Announcements</a>
            `;

        // Call the function to get and display tournament announcements
        console.log(information)
        await showSchedule(information.schedule);
    } catch (err) {
        console.log(err.message);
    }


    const saveBtn = document.getElementById('saveBtn')
    saveBtn.addEventListener("click", () => {
        const team1 = document.getElementById('team1').value;
        const team2 = document.getElementById('team2').value;
        const date = document.getElementById('date').value;

        var data = {
            "team1": team1,
            "team2": team2,
            "date": date
        }
        addSchedule(data)

    })

    const addSchedule = async (data) => {
        const urlParams = new URLSearchParams(window.location.search);
        var tournamentid = urlParams.get("tournamentid");
        try {
            var res = await axios.post("http://localhost:6969/schedule", data);
            console.log(res.data.data._id);
            if (res.data.message === "livestream created") {
                showSuccess("Livestream Added Successfully!");
            }

            var res = await axios.patch('http://localhost:6969/tournament/' + tournamentid, {
                "schedule": res.data.data._id
            })

            console.log(res.data)
            // window.location.reload();
            if (!res.data.status === "success") {
                showError("Schedule Failed To link in Tournament")
            }

        } catch (err) {
            console.log("Bad messafge");
            showError("Could't create schedule");
        }
    }

});








