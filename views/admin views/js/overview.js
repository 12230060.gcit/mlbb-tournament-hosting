document.addEventListener("DOMContentLoaded", () => {
  const getTournamentOverview = async () => {
      try {
          // Parse the URL to extract the tournamentid parameter
          const urlParams = new URLSearchParams(window.location.search);
          const tournamentid = urlParams.get("tournamentid").toString();
          console.log(tournamentid);
          
          // Make a request using tournamentid
          const res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament/${tournamentid}`);
          showTournament(res.data.data);
      } catch (err) {
          console.log(err.message);
      }
  };

  const showTournament = (information) => {
      // Display tournament information
      const tournamentInfo = document.querySelector(".paragraph");
      const teamRegistered = document.querySelector(".team-list");
      const dateInfo = document.querySelector(".description.editable");

      dateInfo.innerHTML = `
          <p>${information.description}</p>
      `;
      tournamentInfo.innerHTML = `
          <p><span class="material-symbols-outlined"> emoji_events</span><span class="editable">${information.prize}</span></p>
          <p><span class="material-symbols-outlined">schedule</span><span class="editable">${information.duration}</span></p>
          <p><span class="material-symbols-outlined">calendar_month</span><span class="editable">${information.date}</span></p>
          <p><span class="material-symbols-outlined">account_tree</span><span class="editable">${information.tournamentType}</span></p>
      `;

      information.teams.forEach(team => {
          // Display registered teams
          teamRegistered.innerHTML += `
              <li>
                  <img src="admin views/img/logoTeam-01 1.png" alt="">
                  ${team.teamName}
              </li>
          `;
      });

      // Display navigation links
      const navContainer = document.querySelector(".inline-nav");
      navContainer.innerHTML = `
          <a href="/editoverview?tournamentid=${information._id}"class="overview">Overview</a>
          <a href="/Admin_structure?tournamentid=${information._id}">Structure</a>
          <a href="/Admin_Schedule?tournamentid=${information._id}">Schedule</a>
          <a href="/stream?tournamentid=${information._id}">Live Streams</a>
          <a href="/announcement?tournamentid=${information._id}">Announcements</a>
      `;
  };

  getTournamentOverview();
});
