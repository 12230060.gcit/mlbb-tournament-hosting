document.addEventListener("DOMContentLoaded", async () => {

    const showStreams = async (streams) => {
        try {

            var liveStreamWrapper = document.querySelector('.liveStreamWrapper');
            console.log(liveStreamWrapper);
            console.log(streams);

            liveStreamWrapper.innerHTML = '';

            for (const stream of streams) {
       
                // Fetch team names corresponding to team1 and team2 ObjectIds
                const team1Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/teams/${stream.team1}`);
                const team2Res = await axios.get(`https://mlbb-tournament-hosting.onrender.com/teams/${stream.team2}`);
       
                // Access team names from the response data
                // console.log(team2Res.data.data.team.teamName)
                const team1Name = team1Res.data.data.team.teamName;
                const team2Name = team2Res.data.data.team.teamName;
                liveStreamWrapper.innerHTML +=
                    `<div class="livestreamContainer">
                        <div class="teamDetails">
                            <div class="team">
                                <p>${team1Name}</p>
                            </div>
                            <div class="link">
                                <p>VS</p>
                                <a href="${streams.streamLink}">Watch Live</a>
                            </div>
                            <div class="team">
                                <p>${team2Name}</p>
                            </div>
                        </div>
                    </div>`;
            }
        } catch (err) {
            console.log(err.message);
        }
    };



    try {
        const urlParams = new URLSearchParams(window.location.search);
        const tournamentid = urlParams.get("tournamentid");
        if (!tournamentid) {
            throw new Error("tournamentid parameter is missing");
        }

        console.log(tournamentid);
        const tournamentRes = await axios.get(`https://mlbb-tournament-hosting.onrender.com/tournament/${tournamentid}`);
        const information = tournamentRes.data.data;

        // Populate the navigation links using the tournament information
        const navContainer = document.querySelector(".inline-nav");
        navContainer.innerHTML = `
                <a href="/editoverview?tournamentid=${information._id}">Overview</a>
                <a href="/Admin_structure?tournamentid=${information._id}">Structure</a>
                <a href="/Admin_Schedule?tournamentid=${information._id}">Schedule</a>
                <a href="/stream?tournamentid=${information._id}"class="overview">Live Streams</a>
                <a href="/announcement?tournamentid=${information._id}">Announcements</a>
            `;

        // Call the function to get and display tournament announcements
        console.log(information)
        await showStreams(information.liveStreams);
    } catch (err) {
        console.log(err.message);
    }


    const saveBtn = document.getElementById('saveBtn')
    saveBtn.addEventListener("click", () => {
        const team1 = document.getElementById('team1Input').value;
        const team2 = document.getElementById('team2Input').value;
        const streamLink = document.getElementById('linkInput').value;

        var data = {
            "team1": team1,
            "team2": team2,
            "streamLink": streamLink
        }
        addStream(data)

    })

    const addStream = async (data) => {
        const urlParams = new URLSearchParams(window.location.search);
        var tournamentid = urlParams.get("tournamentid");
        try {
            var res = await axios.post("https://mlbb-tournament-hosting.onrender.com/stream", data);
            console.log(res.data.data._id);
            if (res.data.message === "livestream created") {
                showSuccess("Livestream Added Successfully!");
            }

            var res = await axios.patch('https://mlbb-tournament-hosting.onrender.com/tournament/' + tournamentid, {
                "liveStreams": res.data.data._id
            })

            console.log(res.data)
            window.location.reload();
            if (!res.data.status === "success") {
                showError("liveStream Failed To link in Tournament")
            }

        } catch (err) {
            console.log("Bad messafge");
            showError("Could't create stream");
        }
    }

});








