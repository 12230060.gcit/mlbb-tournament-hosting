document.addEventListener("DOMContentLoaded", () => {
    const getTournament = async () => {
      try {
        const res = await axios.get("https://mlbb-tournament-hosting.onrender.com/tournament");
        showTournament(res.data.data);
      } catch (err) {
        showError(err.message);
      }
    };
  
    const showTournament = async (home) => {
      console.log(home);
      var homeContainer = document.querySelector(".container");
      home.forEach((home) => {
      console.log(home._id);
        homeContainer.innerHTML += ` 
          <div class="card">
          <div class="imgWrapper">
              <img src="admin views/img/florian-olivo-Mf23RF8xArY-unsplash.jpg" alt="" >
          </div>
          <div class="cardContent">
              <h2>${home.tournamentName}</h2>
              <div class="cardDetails">
                   <p>TournamentType:${home.tournamentType}</p>
                  <p>Date:${home.date}</p>
                  <p>Duration:${home.duration}</p>
              </div>
              <div class="linkWrapper">
                  <a href="/editOverview?tournamentid=${home._id}">View Details</a>
                  
              </div>
              
          </div>
      </div>`;
      });
    };
    getTournament();
  
    const saveBtn = document.getElementById("saveBtn");
    saveBtn.addEventListener("click", (e) => {
      e.preventDefault();
      const tournamentName = document.getElementById("tournamentName").value;
      const durations = document.getElementById("durations").value;
      const date = document.getElementById("date").value;
      const photo = document.getElementById("photo").value;
      const prize = document.getElementById("price").value;
      const description = document.getElementById("description").value;
      const type = document.getElementById("tournamentType").value;
      // Enter validation function call or code here
  
      //
  
      addTournament(tournamentName, durations, date, photo,prize,description,type);
    });
  
    const addTournament = async (
      tournamentName,
      durations,
      date,
      photo,
      prize,
      description,
      type
    ) => {
        var data = {
           "tournamentName" : tournamentName,
            "duration": durations,
            "date" : date,
            "photo": photo,
            "prize": prize,
          "description" : description,
          "tournamentType" : type
          }
          console.log(data)
      try {
        const res = await axios.post("https://mlbb-tournament-hosting.onrender.com/adminhome", data);
        console.log(res);
        if (res.data.message === "Home created") {
          showSuccess("Tournament Added Successfully!");
          getTournament();
        }
      } catch (err) {
        console.log("Bad messaage");
        showError(err);
      }
    };
  });
  