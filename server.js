const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config({ path: "config.env" });
const app = require("./app.js");

const DB = process.env.DATABASE.replace(
    "PASSWORD",
    process.env.DATABASE_PASSWORD
);

mongoose.connect(DB).then((con) => {
    console.log(`Database connected to ${con.connection.host}`);
}).catch(error => console.log(error));

const httpServer = require("http").createServer(app);

const port = 6969;

httpServer.listen(port, () => {
    console.log(`App running on port http://localhost:${port} ..`);
});
