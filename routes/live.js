const express = require("express");
const liveStreams = require("../controller/livestreams");

const router = express.Router();

router
  .route("/")
  .get(liveStreams.getAllStream)
  .post(liveStreams.createStream);
router
  .route("/:id")
  .get(liveStreams.getStream)
  .delete(liveStreams.deleteStream);

module.exports = router;