const express = require("express");
const viewsController = require("../controller/viewController");
const router = express.Router();

router.get("/stream", viewsController.getStreamPage);
router.get('/adminhome',  viewsController.getHome)
router.get("/announcement", viewsController.getAnnouncementPage);
router.get("/Admin_Schedule", viewsController.getSchedule);
router.get("/Admin_structure", viewsController.getStructure);
router.get("/tournamentAnnouncement", viewsController.getTournamentAnnouncementPage);
router.get("/tournamentLivestream", viewsController.getTournamentLivestreamPage);
router.get("/tournamentSchedule", viewsController.getTournamentSchedulePage);
router.get("/tournamentEvents", viewsController.getTournamentEventsPage);
router.get("/tournamentSignup", viewsController.getTourSignupPage);
router.get("/tournamentLogin", viewsController.getTournamentLoginPage);
router.get("/tournamentOverview", viewsController.getTournamentOverview);
router.get("/tournamentStructure", viewsController.getTournamentStructurePage);
router.get("/mainDashboard", viewsController.mainDashboard);
router.get("/joinTeam", viewsController.joinTeam);
router.get("/profleEdit", viewsController.editProfile);
router.get("/profileTournament", viewsController.profileTournament);
router.get("/About", viewsController.AboutUs);
router.get("/profileInfo", viewsController.profileInfo);
router.get("/Event_details", viewsController.register);
router.get("/availableteams", viewsController.availaleteams);
router.get("/details", viewsController.teamMembers);
router.get("/editOverview", viewsController.getOveview);
router.get("/", viewsController.generalDashboard);
module.exports = router;
