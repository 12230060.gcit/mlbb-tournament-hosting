const express = require("express");
const tournament = require("../controller/tournament");

const router = express.Router();

router
  .route("/")
  .get(tournament.getAllTournaments)
  .post(tournament.createTournament);

router
  .route("/:id")
  .get(tournament.getTournament)
  .delete(tournament.deleteTournament)
  .patch(tournament.updateTournament);

module.exports = router;