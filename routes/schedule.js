const express = require("express");
const schedule = require("../controller/schedule");

const router = express.Router();

router
  .route("/")
  .get(schedule.getAllSchedule)
  .post(schedule.createSchedule);
router
  .route("/:id")
  .get(schedule.getSchedule)
  .delete(schedule.deleteSchedule);

module.exports = router;