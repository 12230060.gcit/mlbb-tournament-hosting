const express = require("express");
const announcement = require("../controller/announcement");

const router = express.Router();

router
  .route("/")
  .get(announcement.getAllAnnouncement)
  .post(announcement.createAnnouncement);

router
  .route("/:id")
  .get(announcement.getAnnouncement)
  .delete(announcement.deleteAnnouncement);

module.exports = router;