
const express = require("express");
const team = require("../controller/team");

const router = express.Router();

router
  .route("/")
  .get(team.getAllTeams)
  .post(team.createTeam);
router
  .route("/:id")
  .patch(team.updateTeam)
  .get(team.getTeam)
  .delete(team.deleteTeam);

module.exports = router;