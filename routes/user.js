const express = require("express");
const userController = require("../controller/user");
const router = express.Router();

router.post("/login", userController.login);
router.post("/logout", userController.logout);
router.post("/signup", userController.createUser);
// router.post("/updateMe", userController.updateMe);
router.post("/updateMyPassword", userController.updatePassword);
router.patch("/updateProfile", userController.protect,userController.updateMe);
router.patch(
  "/changePassword",
  userController.protect,
  userController.changePassword
);


router
  .route("/")
  .get(userController.getAllUser)
  .post(userController.createUser);

router
  .route("/:id")
  .get(userController.getUser)
  .patch(userController.updateUser)
  .delete(userController.deleteUser);

router.post("/login", userController.login);
module.exports=router;