const express = require("express");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();

app.use(cors());
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.json());
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, "views")));
const viewRoute = require("./routes/viewRoutes");
app.use("/", viewRoute);
// Users
const userRoute = require("./routes/user");
app.use("/userdb", userRoute);

// live stream
const streamRoute = require("./routes/live");
app.use("/stream", streamRoute);

//Announcement
const annRoute = require("./routes/announcement");
app.use("/annouc", annRoute);

const admindb = require("./routes/adminROutes")
app.use("/admindb", admindb)

const schedule = require("./routes/schedule")
app.use("/schedule", schedule)


// Announcement
const AnnouncementRoute = require("./routes/announcement");
app.use("/announcement", AnnouncementRoute);

// overview
// const overviewRoutes = require("./routes/overviewRoutes");
// app.use("/overviewdb", overviewRoutes);


const tournamentRoute = require("./routes/tournament");
app.use('/tournament', tournamentRoute)

const homeRoute = require("./routes/tournament");
app.use('/adminhome', homeRoute)

const teamRoute = require("./routes/team");
app.use('/teams', teamRoute)


module.exports = app;
