const path = require("path");

exports.getStreamPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "livee.html"));
};

exports.getHome = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "home.html"))
}
exports.teamMembers = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "detail.html"))
}


exports.getAnnouncementPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "announc.html"));
};
exports.getSchedule = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "Admin_Schedule.html"));
};
exports.getOveview = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "overview.html"));
};
exports.getStructure = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/admin views/", "Admin_structure.html"));
};




exports.getTournamentAnnouncementPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentAnnouncement.html"));
};
exports.getTournamentOverview = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentOverview.html"));
};

exports.getTournamentLivestreamPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentLivestream.html"));
};


exports.getTournamentSchedulePage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentSchedule.html"));
};
exports.getTournamentEventsPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentEvents.html"));
};
exports.getTournamentStructurePage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "tournamentStructure.html"));
};

exports.getTourSignupPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend/", "signup.html"));
};
exports.getTournamentLoginPage = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend/", "login.html"));
};
exports.mainDashboard = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "Main_DashBoard.html"));
};
exports.generalDashboard = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "General_Dashboard.html"));
};

exports.joinTeam = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "Join Team.html"));
};
exports.editProfile = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "Profile_edit.html"));
};
exports.profileTournament = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "profile_Tour.html"));
};
exports.AboutUs = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "About.html"));
};
exports.profileInfo = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "profile.html"));
};
exports.register = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "Event_details.html"));
};
exports.availaleteams = (req, res) => {
  res.sendFile(path.join(__dirname, "../", "views/userFrontend", "Profile_Available_Teams.html"));
};
exports.get;
