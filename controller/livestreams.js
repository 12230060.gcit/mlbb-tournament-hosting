const Stream = require("./../model/stream");
const team = require("./../model/teams");

exports.getAllStream = async (req, res, next) => {
  try {
    const stream = await Stream.find();
    res.status(200).json({ data: stream, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.createStream = async (req, res, next) => {
  try {
    console.log(req.body);
    const stream = await Stream.create(req.body);
    res.status(200).json({ data: stream, message: "Stream created" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.getStream = async (req, res, next) => {
  try {
    const stream = await Stream.findById(req.params.id).populate("team1").populate("team1");
    res.status(200).json({ data: stream, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.deleteStream = async (req, res, next) => {
  try {
    const stream = await Stream.findByIdAndDelete(req.params.id);
    res.status(200).json({ data: stream, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
