const User = require("./../model/usermodel");
const path = require("path");
const jwt = require("jsonwebtoken");
const AppError = require("./../utils/appError");
const teams = require("./../model/teams");
const bcrypt=require("bcryptjs")

const { promisify } = require("util");

exports.getAllUser = async (req, res, next) => {
  try {
    const user = await User.find().populate("teams");
    res.status(200).json({ data: user, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.createUser = async (req, res, next) => {
  try {
    const { name, email, password, passwordConfirm } = req.body;

    // Log incoming data for debugging
    console.log("Received data:", { name, email, password, passwordConfirm });

    if (!name || !email || !password || !passwordConfirm) {
      return res.status(400).json({ error: "All fields are required" });
    }

    if (password !== passwordConfirm) {
      return res.status(400).json({ error: "Passwords do not match" });
    }

    const user = await User.create({ name, email, password, passwordConfirm });
    res.status(200).json({ data: user, message: "User created" });
  } catch (error) {
    console.error("Error creating user:", error);
    res.status(500).json({ error: error.message });
  }
};

exports.getUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id).populate("teams")
    res.status(200).json({ data: user, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.updateUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { email, ...updatedFields } = req.body;

    if (email) {
      const existingUser = await User.findOne({ email });
      if (existingUser && existingUser._id.toString() !== id) {
        return res.status(400).json({ error: "Email already exists" });
      }
    }

    const user = await User.findByIdAndUpdate(id, updatedFields, { new: true });

    res.status(200).json({ data: user, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.deleteUser = async (req, res, next) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    res.status(200).json({ data: user, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};



const signToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

const createSendToken = (user, statusCode, res) => {
  const token = signToken(user._id);
  console.log(token.name)

  const cookieOptions = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 1000
    ),
    httpOnly: true,
  };
  res.cookie("jwt", token, cookieOptions); 
  res.status(statusCode).json({
    status: "Success",
    token,
    data: {
      user,
    },
  });
};


exports.logout = (req, res) => {
  res.cookie("jwt", "", {
    expires: new Date(Date.now() - 10 * 1000),
    httpOnly: true,
  });
  res.status(200).json({ status: "success" });
};

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return next(new AppError("Please provide an email and password", 400));
    }

    const user = await User.findOne({ email }).select("+password");

    if (!user || !(await user.correctPassword(password, user.password))) {
      return next(new AppError("Incorrect email or password", 401));
    }

    createSendToken(user, 200, res);
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
};

exports.protect = async (req, res, next) => {
  try {
    let token;

    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith("Bearer")
    ) {
      token = req.headers.authorization.split(" ")[1];
    } else if (req.cookies.jwt) {
      token = req.cookies.jwt;
    }

    if (!token) {
      return next(new AppError("You are not logged in!! Please Log in", 401));
    }

    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    const freshUser = await User.findById(decoded.id);

    if (!freshUser) {
      return next(
        new AppError("The user belonging to this token no longer exists", 401)
      );
    }

    req.user = freshUser;
    next();
  } catch (err) {
    return next(new AppError("Invalid token. Please log in again",401));
}
};

const filterObj = (obj, ...allowedFields) => {
  const newObj = {}
  Object.keys(obj).forEach((el) => {
      if (allowedFields.includes(el)) newObj[el] = obj[el]
  })
  return newObj
}

// exports.updateMe = async (req, res, next) => {
//   try {
//       //1) create error if user posts password data
//       if (req.body.password || req.body.passwordConfirm) {
//           return next(
//               new AppError('This route is not for password udates. Please use /updateMyPassword', 400,),
//           )
//       }
//       //2) filter out unwanted fields names that are not allowed to be updated
//       const filteredBody = filterObj(req.body, 'name')

//       var obj = JSON.parse(req.cookies.token)
//       console.log(obj)
//       const updatedUser = await User.findByIdAndUpdate(obj['userId'], filteredBody, {
//           new: true,
//           runValidators: true,
//       })

//       res.status(200).json({
//           status: 'success',
//           data: { user: updatedUser },
//       })

//   } catch (err) {
//       res.status(500).json({ error: err.message });
//   }
// }




exports.updatePassword = async (req, res, next) => {
  try {
      //1 Getting user from collection
      console.log(req.body)
      const user = await User.findById(req.user.id).select('+password')

      //check if posted current password is correct
      if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
          return next(new AppError('Your current password is wrong', 401))
      }

      //3 If so , update password
      user.password = req.body.password
      user.passwordConfirm = req.body.passwordConfirm

      //4Log user in and sent JWT
      createSendToken(user, 200, res)

  } catch (err) {
      res.status(500).json({ error: err.message })
  }
}

exports.changePassword = async (req, res, next) => {
  try {
    const { currentPassword, password, passwordConfirm } = req.body;

    // Validate request body
    if (!currentPassword || !password || !passwordConfirm) {
      return res.status(400).json({
        status: "Failed",
        message:
          "Please provide current password, new password, and confirm password",
      });
    }

    // Check if new password and confirmation match
    if (password !== passwordConfirm) {
      return res.status(400).json({
        status: "Failed",
        message: "Passwords do not match",
      });
    }

    // Validate new password length
    if (password.length < 8) {
      return res.status(400).json({
        status: "Failed",
        message: "New password must be at least 8 characters long",
      });
    }

    // Ensure the user is authenticated and loaded by the middleware
    const user = await User.findById(req.user._id).select("+password");

    if (!user) {
      return res.status(404).json({
        status: "Failed",
        message: "User not found",
      });
    }

    // Verify the current password
    const verified = await bcrypt.compare(currentPassword, user.password);
    if (!verified) {
      return res.status(400).json({
        status: "Failed",
        message: "Invalid current password",
      });
    }

    // Update the password
    user.password = password;
    user.passwordConfirm = passwordConfirm;

    await user.save();

    // Generate a new JWT token
    const jwtToken = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
      expiresIn: "30d",
    });

    // Respond with success
    res.status(200).json({
      status: "Success",
      results: { jwtToken },
    });
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({
      status: "Failed",
      message: "An error occurred while changing password",
      error: error.message,
    });
  }
};
exports.updateMe = async (req, res, next) => {
  const { name } = req.body;
  const newUser = { name };
  const updatedUser = await User.findByIdAndUpdate(req.user._id, newUser, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({ status: "Success", results: { updatedUser}});
};
