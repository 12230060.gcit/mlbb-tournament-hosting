const User = require("../model/usermodel")

exports.updatePassword = async (req, res, next) => {
    try {
        //1 Getting user from collection
        const user = await User.findById(req.user.id).select('+password')

        //check if posted current password is correct
        if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
            return next(new AppError('Your current password is wrong', 401))
        }

        //3 If so , update password
        user.password = req.body.password
        user.passwordConfirm = req.body.passwordConfirm

        //4Log user in and sent JWT
        createSendToken(user, 200, res)

    } catch (err) {
        res.status(500).json({ error: err.message })
    }
}
