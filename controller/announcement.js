const Announcement = require("./../model/announcement");

const { promisify } = require("util");

exports.getAllAnnouncement = async (req, res, next) => {
  try {
    const announcement = await Announcement.find();
    res.status(200).json({ data: announcement, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.createAnnouncement = async (req, res, next) => {
  try {
    const announcement = await Announcement.create(req.body);
    res
      .status(200)
      .json({ data: announcement, message: "Announcement created" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.getAnnouncement = async (req, res, next) => {
  try {
    const announcement = await User.findById(req.params.id).populate({
      path: "partOfGroup",
      populate: "groupTitle",
    });
    res.status(200).json({ data: announcement, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.updateAnnouncement = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { ...updatedFields } = req.body;

    const announcement = await Announcement.findByIdAndUpdate(
      id,
      updatedFields,
      { new: true }
    );

    res.status(200).json({ data: announcement, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.deleteAnnouncement = async (req, res, next) => {
  try {
    const announcement = await Announcement.findByIdAndDelete(req.params.id);
    res.status(200).json({ data: announcement, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
