const Tournament = require("./../model/tournament");
const Announcement = require("./../model/announcement");
const Schedule = require("./../model/schedule");
const Stream = require("./../model/stream");
// const Team = require("./../model/teams");
const Team = require("./../model/teams")

exports.getAllTournaments = async (req, res, next) => {
  try {
    const tournaments = await Tournament.find().populate("teams").populate('announcement').populate('schedule').populate('liveStreams');
    res.status(200).json({ data: tournaments, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.createTournament = async (req, res, next) => {
  try {
    const tournament = await Tournament.create(req.body);
    res
      .status(200)
      .json({ data: tournament, message: "Tournament created" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.getTournament = async (req, res, next) => {
  try {
    const tournament = await Tournament.findById(req.params.id).populate("teams").populate('announcement').populate('schedule').populate('liveStreams');
    res.status(200).json({ data: tournament, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
exports.updateTournament = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { teams, announcement, schedule, liveStreams, ...updatedFields } = req.body;

    const updateOps = {};

    // Conditionally handle the array fields
    if (teams) {
      updateOps.$push = { teams: { $each: Array.isArray(teams) ? teams : [teams] } };
    }
    if (announcement) {
      if (!updateOps.$push) updateOps.$push = {};
      updateOps.$push.announcement = { $each: Array.isArray(announcement) ? announcement : [announcement] };
    }

    if (schedule) {
      console.log(schedule ,"schedule");
      if (!updateOps.$push) updateOps.$push = {};
      updateOps.$push.schedule = { $each: Array.isArray(schedule) ? schedule : [schedule] };
    }
    if (liveStreams) {
      if (!updateOps.$push) updateOps.$push = {};
      updateOps.$push.liveStreams = { $each: Array.isArray(liveStreams) ? liveStreams : [liveStreams] };
    }

    // Handle other fields
    Object.assign(updateOps, updatedFields);

    // Update the tournament
    let tournament = await Tournament.findByIdAndUpdate(id, updateOps, { new: true });

    if (!tournament) {
      return res.status(404).json({ message: "Tournament not found" });
    }

    // Populate the referenced fields
    tournament = await Tournament.findById(id)
      .populate('teams')
      .populate('announcement')
      .populate('schedule')
      .populate('liveStreams');

    res.status(200).json({ data: tournament, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};


exports.deleteTournament = async (req, res, next) => {
  try {
    const tournament = await Tournament.findByIdAndDelete(req.params.id);
    res.status(200).json({ data: tournament, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
