const Schedule = require("../model/schedule");

const { promisify } = require("util");

exports.getAllSchedule = async (req, res, next) => {
  try {
    const schedule = await Schedule.find();
    res.status(200).json({ data: schedule, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.getSchedule = async (req, res, next) => {
  try {
    const schedule = await Schedule.findById(req.params.id);
    res.status(200).json({ data: schedule, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.createSchedule = async (req, res, next) => {
  try {
    console.log(req.body);
    const schedule = await Schedule.create(req.body);
    res.status(200).json({ data: schedule, message: "Schedule created" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.updateSchedule = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { ...updatedFields } = req.body;

    const schedule = await Schedule.findByIdAndUpdate(id, updatedFields, {
      new: true,
    });

    res.status(200).json({ data: schedule, status: "success" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
exports.deleteSchedule = async (req, res, next) => {
    try {
      const schedule = await schedule.findByIdAndDelete(req.params.id);
      res.status(200).json({ data: schedule, status: "success" });
    } catch (error) {
      res.status(500).json({ error: error.message });
    }
  };