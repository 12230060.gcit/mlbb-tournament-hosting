const Team = require("../model/teams");
const users = require("../model/usermodel");
// Create a new team
exports.createTeam = async (req, res) => {
  try {
    const team = await Team.create(req.body);
    res.status(201).json({
      status: "success",
      data: {
        team,
      },
    });
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err.message,
    });
  }
};

// Get all teams
exports.getAllTeams = async (req, res) => {
  try {
    const teams = await Team.find().populate("members");
    res.status(200).json({
      status: "success",
      results: teams.length,
      data: {
        teams,
      },
    });
  } catch (err) {
    res.status(500).json({
      status: "fail",
      message: err.message,
    });
  }
};

// Get a single team by ID
exports.getTeam = async (req, res) => {
  try {
    const team = await Team.findById(req.params.id);
    res.status(200).json({
      status: "success",
      data: {
        team,
      },
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      message: "Team not found",
    });
  }
};

exports.updateTeam = async (req, res) => {
  try {
    const { members, ...otherUpdates } = req.body;

    // Check if the request includes members to update
    if (members) {
      // If members are provided, update the members of the team
      const team = await Team.findByIdAndUpdate(
        req.params.id,
        { $addToSet: { members: { $each: members } } }, // Add new members to the team
        { new: true, runValidators: true }
      );
      res.status(200).json({
        status: "success",
        data: {
          team,
        },
      });
    } else {
      // If no members provided, update other fields of the team
      const team = await Team.findByIdAndUpdate(req.params.id, otherUpdates, {
        new: true,
        runValidators: true,
      });
      res.status(200).json({
        status: "success",
        data: {
          team,
        },
      });
    }
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err.message,
    });
  }
};
// Delete a team by ID
exports.deleteTeam = async (req, res) => {
  try {
    await Team.findByIdAndDelete(req.params.id);
    res.status(204).json({
      status: "success",
      data: null,
    });
  } catch (err) {
    res.status(404).json({
      status: "fail",
      message: "Team not found",
    });
  }
};
