const mongoose = require("mongoose");
const Announcement = require("./announcement");
const Schedule = require("./schedule");
const Stream = require("./stream");
const Team = require("./teams");

const tournamentSchema = new mongoose.Schema({
  tournamentName: {
    type: String,
    required: [true, "Please provide the tournament name"],
  },
  description: {
    type: String,
    required: [true, "Please provide the tournament description"],
  },
  duration: {
    type: String,
    required: [true, "Please provide the tournament duration"],
  },
  date: {
    type: Date,
    required: [true, "Please provide the tournament start date"],
  },
  tournamentType: {
    type: String,
    required: [true, "Please provide the tournament type"],
  },
  status: {
    type: String,
    required: [true, "Please provide the tournament status"],
    default: "Ongoing",
  },
  prize: {
    type: String,
    required: [true, "Please provide the tournament prize"],
  },
  teams: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Teams' }],
    required: [true, "Please provide the list of teams"],

  },
  announcement: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Announcement' }],
    required: [true, "Please provide the tournament announcement"],
  },
  schedule: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Schedule' }],
    required: [true, "Please provide the tournament schedule"],
  },
  liveStreams: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Stream' }],
    required: [true, "Please provide the list of live streams"],
  },   
});

const tour = mongoose.model("tournament", tournamentSchema);
module.exports =tour;