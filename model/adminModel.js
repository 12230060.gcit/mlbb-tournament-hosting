const mongoose = require ('mongoose')
const bcrypt = require('bcryptjs')
const validator = require("validator")

const adminSchema = new mongoose.Schema ({
     email: {
        type: String,
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide a valid email"],},
    password:{
        type: String,
        required: [true, 'please provide a password'],
        minlength: 8,
    }
})
adminSchema.pre('save', async function (next){
    if (!this.isModified('password')) return next()
    this.password = await bcrypt.hash(this.password, 12)
    next()
})

adminSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}
const Admin = mongoose.model('Admin', adminSchema)
module.exports=Admin
