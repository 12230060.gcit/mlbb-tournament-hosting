const mongoose = require("mongoose");

const teamsSchema = new mongoose.Schema({
  teamName: {
    type: String,
    required: [true, "Please tell us your teamname!"],
  },
  logo: {
    type: String,
    default: "teamLogo.png",
    // required: [true, "what is the durations"],
  },
  members: {
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    required: [true, "Please provide the list user in you team"],
  },
});

const teams = mongoose.model("Teams", teamsSchema);
module.exports =teams;