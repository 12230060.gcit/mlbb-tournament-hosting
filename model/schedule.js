const mongoose = require("mongoose");

const scheduleSchema = new mongoose.Schema({
  team1: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: [true, "Please tell us your team name!"],
  },
  team2: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Team',
    required: [true, "Please tell us your team name!"],
  },
  date: {
    type: String,
    required: [true, "Time for the match"],
  },
});

const Schedule = mongoose.model("Schedule", scheduleSchema);
module.exports = Schedule;
